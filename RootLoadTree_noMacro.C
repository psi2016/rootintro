

/*
 * Examples to show how to load trees
 * Authors: Simon Corrodi, Dorothea vom Bruch
 * August 2014


  Ä Ä
 (* *)
  \ö/
   |-------|\
   |       | \
   |       |

 */

{
  /*
   * Load data from file
   * TFile(const char* fname, Option_t* option = "", const char* ftitle = "", Int_t compress = 1)
   */

  TFile *file = new TFile("scan.root","READ");

  /* To Do
   *
   * The easiest way to get a first look at a root file is the TBrowser (http://root.cern.ch/root/html/TBrowser.html)
   * Typing 'new TBrowser' into the Root terminal opens it.
   * Explore the file scan.root: It contains a histogram ('statFrames') and a TBranch ('Coin') with four branches:
   * energy1, energy2, deltaTcc, deltaTfine).
   * Explore the editor: View->Editor and the Statusbar: View->Event Statusbar
   * Many options are available by clicking / right clicking onto various parts of the canvas within the TBrowser.
   */

  /*
   * Load the tree, show the # of entries and its branches
   */
  TTree *tree = (TTree*)file->Get("Coin");                // Get tree called 'coin'
  TObjArray* branchList = tree->GetListOfBranches();      // Get Array with branches contained in tree
  cout << "Total entries: " << tree->GetEntries() << endl;
  cout << "Branches: " << endl;
  for (int i = 0; i < 4; i++) {
    cout << " -" << branchList->At(i)->GetName() << endl;  // Get name of each branch
  }

  /*
   * Plot energy histograms
   */


  TCanvas *c1 = new TCanvas("c1","My energies",1200,1000);
  c1->Divide(2,2);

  c1->cd(1);
  tree->Draw("energy1");

  c1->cd(2);
  tree->Draw("energy2>>he2(601,0,600)");       // Create histogram 'he2' with 601 bins from 0 to 600, it is save in global gDirectory

  /* By default, histograms created by tree->Draw() are called htemp.
   * By adding ">>hname" to the drawing command, the histogram is called
   * hname and stored in gDirectory.
   *
   * Load histograms form gDirectory
   */

  TH1F *he2 = (TH1F*)gDirectory->Get("he2");
  TH1F *he1 = (TH1F*)gROOT->FindObject("htemp")->Clone("he1");

  /*
   * Scatter Plots
   *
   * Draw correlations between the two energy channels
   * Draw("x:y") plots x versus y
   * Third option is the drawing option.
   */

  c1->cd(3);
  tree->Draw("energy1:energy2>>he(601,0,600,601,0,600)","","zcol");   // scatter plot with color
  he->GetXaxis()->SetTitle("energy1");
  he->GetYaxis()->SetTitle("energy2");

  /*
   * Create an alias
   */

  tree->SetAlias("deltaT","deltaTfine*51");    // deltaTfine is given in 51 ps bins
  c1->cd(4);
  tree->Draw("deltaT>>ht(117,-3000,3000)");
  ht->GetXaxis()->SetTitle("deltaT [ps]");

  /*
   * Repition of Gaus fit
   */
  TF1 *ge1 = new TF1("ge1","gaus",420,520);
  TF1 *ge2 = new TF1("ge2","gaus",380,500);

  gStyle->SetOptFit(1111);    // Display fit statistics

  c1->cd(1);
  he1->Fit("ge1","R");

  c1->cd(2);
  he2->Fit("ge2","R");

  /*
   * Cuts
   */

  /*
   * Plot deltaT of Photopeak events
   */

  c1->cd(4);
  double energy1_lower = ge1->GetParameter("Mean") - 1.5 * ge1->GetParameter("Sigma");   // get upper and lower bounds
  double energy1_upper = ge1->GetParameter("Mean") + 1.5 * ge1->GetParameter("Sigma");
  double energy2_lower = ge2->GetParameter("Mean") - 1.5 * ge2->GetParameter("Sigma");
  double energy2_upper = ge2->GetParameter("Mean") + 1.5 * ge2->GetParameter("Sigma");

  tree->Draw("deltaT>>ht2(117,-3000,3000)", Form("energy1 > %f && energy1 < %f && energy2 > %f && energy2 < %f",energy1_lower, energy1_upper, energy2_lower, energy2_upper));

  /*
   * Indicate the cuts in the scatter plot
   * from http://root.cern.ch/root/html/TLine.html
   * TLine(Double_t x1, Double_t y1, Double_t x2, Double_t y2)
   */

  c1->cd(3);
  TLine *l11 = new TLine(0,energy1_lower,600,energy1_lower);
  TLine *l12 = new TLine(0,energy1_upper,600,energy1_upper);
  TLine *l21 = new TLine(energy2_lower,0,energy2_lower,600);
  TLine *l22 = new TLine(energy2_upper,0,energy2_upper,600);
  l11->Draw();
  l12->Draw();
  l21->Draw();
  l22->Draw();

  /*
   * Alternative way to Fill Histograms
   *
   * by looping through all events
   */

  TCanvas *c2 = new TCanvas("c2","",800,600);

  TH1F *tnew = new TH1F("tnew","CTR",117,-3000,3000);
  int deltaTfine, energy1, energy2;

  tree->SetBranchAddress("deltaTfine",&deltaTfine);
  tree->SetBranchAddress("energy1",&energy1);
  tree->SetBranchAddress("energy2",&energy2);

  for (int i = 0; i < tree->GetEntries(); i ++) {
    tree->GetEntry(i);
    if (energy1 > energy1_lower && energy1 < energy1_upper && energy2 > energy2_lower && energy2 < energy2_upper) {
      tnew->Fill(deltaTfine*51);
    }
  }
  tnew->GetXaxis()->SetTitle("delta T [ns]");
  tnew->Draw();

}
